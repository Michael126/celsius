package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

import sheridan.Temperature;

public class CelsiusTest {

	@Test
	public void testTemperatureFromFahrenHeit() {
	assertTrue("Invalid conversion", Temperature.convertFromFahrenheit(2) == -17);
	
	
	
	
	}
	@Test
	public void testTemperatureFromFahrenHeitExceptional() {
	assertTrue("Invalid conversion", Temperature.convertFromFahrenheit(5) != -13);
	
	
	
	}
	@Test
	public void testTemperatureFromFahrenHeitBoundaryIn() {
	assertTrue("Invalid conversion", Temperature.convertFromFahrenheit(6) == -14);
	
	
	
	
	}
	@Test
	public void testTemperatureFromFahrenHeitBoundaryOut() {
	assertTrue("Invalid conversion", Temperature.convertFromFahrenheit(7) == -14);
	
	
	
	
	}
}
